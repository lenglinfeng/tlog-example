package synclog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AsyncAnnoDomain {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Async
    public void testAnnotationAsync(){
        log.info("这是异步标签日志");
    }
}
