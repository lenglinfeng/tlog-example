package com.example.customlabel;

import com.example.customlabel.vo.Person;
import com.yomahub.tlog.core.annotation.TLogAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestDomain {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TestDomain2 testDomain2;

    @TLogAspect(value = "person.address.company")
    public void testMethod1(Person person){
        log.info("这是自定义标签测试方法日志1");
        testDomain2.testMethod(54000);
        log.info("这是自定义标签测试方法日志2");
        log.info("这是自定义标签测试方法日志3");
    }

    @TLogAspect(convert = TestConverter.class)
    public void testMethod2(Person person){
        log.info("这是自定义Convert测试方法日志");
    }

    @TLogAspect(str = "XXX")
    public void testMethod3(Person person){
        log.info("这是自定义字符串常量测试方法日志");
    }
}
