package com.example.mdc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableAsync
@RestController
public class RunnerApplication {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private ExecutorService pool = Executors.newFixedThreadPool(5);

    @Resource
    private AsyncAnnoDomain asyncAnnoDomain;

    public static void main(String[] args) {
        SpringApplication.run(RunnerApplication.class, args);
    }

    @RequestMapping("/hi")
    public String sayHello(@RequestParam String name){
        log.info("invoke method sayHello,name={}",name);
        asyncAnnoDomain.testAnnotationAsync();
        new AsynDomain().start();
        pool.submit(new AsynPoolDomain());
        return "hello";
    }
}
