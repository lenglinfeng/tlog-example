package com.yomahub.tlog.example.dubbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource(locations = {"classpath:/applicationContext.xml"})
public class Runner {

//    static {AspectLogEnhance.enhance();}

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
        while (true) {
            try {
                Thread.sleep(60000);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}
