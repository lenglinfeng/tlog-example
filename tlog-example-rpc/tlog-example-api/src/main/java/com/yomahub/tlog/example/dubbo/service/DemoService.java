package com.yomahub.tlog.example.dubbo.service;

public interface DemoService {

	String sayHello(String name);
}
