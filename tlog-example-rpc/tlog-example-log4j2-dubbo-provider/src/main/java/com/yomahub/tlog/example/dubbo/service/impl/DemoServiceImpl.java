package com.yomahub.tlog.example.dubbo.service.impl;

import com.yomahub.tlog.example.dubbo.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service("demoService")
public class DemoServiceImpl implements DemoService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private ExecutorService pool = Executors.newFixedThreadPool(5);

    @Override
    public String sayHello(String name) {
        log.info("log4j2-dubbo-provider:invoke method sayHello,name={}", name);
        new AsynDomain().start();
        pool.submit(new AsynPoolDomain());
        return "hello," + name;
    }
}